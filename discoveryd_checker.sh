#!/bin/bash

# Copyright (c) 2015, James Swineson <jamesswineson@gmail.com>
# 
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.
# 
# Gist URL: https://gist.github.com/Jamesits/f5fb11af7c1b5cdd292b
# My blog post (in Chinese): 
# http://blog.swineson.me/restart-os-x-10-10-3-buggy-discoveryd-automatically/

# Check if root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

# Process max cpu usage arguments
max_usage=80.0
re='^[0-9]+([.][0-9]+)?$'
if [[ $1 =~ $re ]] ; then
	echo "Max allowed CPU usage:" $1
	max_usage=$1
fi

# Check real CPU usage
while true; do
	cpu_usage=$(ps -Aco command,pcpu | grep "discoveryd " | awk '{$1=""; sub(" ", ""); print}')
	
	if [ "${cpu_usage:-0}" \> $max_usage ] ; then
		echo "Warning: discoveryd CPU Usage at" $cpu_usage ", killing..."
		sudo killall discoveryd
	fi
	
	sleep 5
done